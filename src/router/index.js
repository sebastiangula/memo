import Vue from 'vue'
import Router from 'vue-router'
import Hello from 'pages/Hello'
import Wordlist from 'pages/Wordlist'
import NewWordlist from 'pages/NewWordlist'
import Register from 'pages/Register'
import Login from 'pages/Login'
import { requireAuth } from 'utils/auth'
import Wordlists from 'pages/Wordlists'
import DefaultNavbar from 'components/DefaultNavbar'
import TypingGame from 'pages/TypingGame'
import GameNavbar from 'components/GameNavbar'
import Profile from 'pages/Profile'
import Logout from 'pages/Logout'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/register',
      name: 'register',
      components: { default: Register, navbar: DefaultNavbar }
    },
    {
      path: '/wordlist/new',
      name: 'wordlist',
      components: { default: NewWordlist, navbar: DefaultNavbar },
      beforeEnter: requireAuth
    },
    {
      path: '/wordlist/:id',
      name: 'wordlist',
      components: { default: Wordlist, navbar: DefaultNavbar },
      props: {
        withStatus: false
      },
      beforeEnter: requireAuth
    },
    {
      path: '/',
      name: 'hello',
      components: { default: Hello, navbar: DefaultNavbar }
    },
    {
      path: '/login',
      name: 'login',
      components: { default: Login, navbar: DefaultNavbar }
    },
    {
      path: '/logout',
      name: 'logout',
      component: Logout
    },
    {
      path: '/wordlists',
      name: 'wordlists',
      components: { default: Wordlists, navbar: DefaultNavbar },
      beforeEnter: requireAuth
    },
    {
      path: '/game/typing/:id',
      name: 'typingGame',
      components: { default: TypingGame, navbar: GameNavbar },
      beforeEnter: requireAuth
    },
    {
      path: '/profile/:id',
      name: 'profile',
      components: { default: Profile, navbar: DefaultNavbar }
    }
  ],
  mode: 'history'
})

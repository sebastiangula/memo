import EventBus from './bus.js'

const TOKEN_KEY = 'token'
const TOKEN_LAST_REFRESH_KEY = 'token_time'
const TOKEN_TIME_LIMIT = 290 * 1000
const USERNAME_KEY = 'username'

var MemoAuth = {}

export default MemoAuth.install = function (Vue, options) {
  function setToken (value) {
    localStorage.setItem(TOKEN_LAST_REFRESH_KEY, Date.now())
    localStorage.setItem(TOKEN_KEY, value)
  }

  function clearToken () {
    localStorage.removeItem(TOKEN_KEY)
  }

  Vue.prototype.login = function (login, password) {
    this.axios.post('/rest-auth/login/', {
      username: login,
      password: password
    }).then(function (response) {
      setToken(response.data['token'])
      localStorage.setItem(USERNAME_KEY, login)
      window.setTimeout(function () { this.refreshToken() }.bind(this), TOKEN_TIME_LIMIT)
      this.$router.push('/')
      EventBus.$emit('login')
    }.bind(this)).catch(function (error) {
      // Pattern to check if nested property exists
      var NonFieldErrors = ((((error || {}).response || {}).data || {}).non_field_errors || {})
      if (Object.keys(NonFieldErrors).length !== 0) {
        EventBus.$emit('alert', error.response.data.non_field_errors[0])
      } else {
        EventBus.$emit('alert', 'Unexpected error has occured')
      }
    })
  }
  Vue.prototype.logout = function () {
    clearToken()
    localStorage.removeItem(USERNAME_KEY)
    this.$router.push('/')
    EventBus.$emit('logout')
  }

  Vue.prototype.checkIfLoggedIn = function () {
    if (localStorage.getItem(TOKEN_KEY) && localStorage.getItem(TOKEN_LAST_REFRESH_KEY) <= (Date.now() + TOKEN_TIME_LIMIT)) {
      return true
    } else {
      return false
    }
  }

  Vue.prototype.getUsername = function () {
    return localStorage.getItem(USERNAME_KEY)
  }

  Vue.prototype.getToken = function () {
    return localStorage.getItem(TOKEN_KEY)
  }

  Vue.prototype.refreshToken = function () {
    if (this.checkIfLoggedIn()) {
      this.axios.post('/api-token-refresh/', {'token': this.getToken()}).then(function (response) {
        setToken(response.data['token'])
        EventBus.$emit('login')
      }).catch(function (error) {
        if (error.response.data.non_field_errors[0] === 'Signature has expired.') {
          this.logout()
        }
      }.bind(this))
      window.setTimeout(function () { this.refreshToken() }.bind(this), TOKEN_TIME_LIMIT)
      return true
    } else {
      this.logout()
      return false
    }
  }
}

export function requireAuth (to, from, next) {
  if (localStorage.getItem(TOKEN_KEY) && localStorage.getItem(TOKEN_LAST_REFRESH_KEY) <= (Date.now() + TOKEN_TIME_LIMIT)) {
    next()
  }
}

if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.use(MemoAuth)
}

// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import BootstrapVue from 'bootstrap-vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VeeValidate from 'vee-validate'
import VueEvents from 'vue-events'
import MemoAuth from 'utils/auth'

var axiosInstance = {}
if (process.env.NODE_ENV === 'production') {
  axiosInstance = axios.create({
    baseURL: 'https://memo-backend.herokuapp.com',
    headers: {'content-type': 'application/json'}
  })
} else {
  axiosInstance = axios.create({
    baseURL: 'http://localhost:8000',
    headers: {'content-type': 'application/json'}
  })
}

Vue.use(MemoAuth)

Vue.use(VueAxios, axiosInstance)

Vue.use(VueEvents)

Vue.use(router)

// Globally register bootstrap-vue components
Vue.use(BootstrapVue)
Vue.use(VeeValidate)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
